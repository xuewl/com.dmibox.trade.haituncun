## 项目简介
基于haidaov2.0_beta仿站，该项目为客户定制版。
> 项目周期：20151130~20151215

### 页面列表
- 首页：http://www.haituncun.com/ 
- 列表页：http://www.haituncun.com/ymuyin/
- 详情页：http://www.haituncun.com/muyin/bbhl/1002426/
- 登陆注册：http://www.haituncun.com/customer/account/login
- 帮助页：http://www.haituncun.com/helper/faq/#about-price

### 其它要求
模板尺寸：
标准宽度1200PX 首页幻灯片宽度1920PX

模板主色：
\#FF5500 ■■■

可参考淘宝的色调
